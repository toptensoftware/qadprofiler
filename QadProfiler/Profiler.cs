﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace QadProfiler
{
    public static class Profiler
    {
        static Profiler()
        {
            _defaultLog = new ProfileLog("QadLog.bin");
        }

        static ProfileLog _defaultLog;

        [Conditional("PROFILE")]
        public static void Enter(string name)
        {
            _defaultLog.Enter(name);
        }

        [Conditional("PROFILE")]
        public static void Tag(string name)
        {
            _defaultLog.Tag(name);
        }


        [Conditional("PROFILE")]
        public static void Mark(string name)
        {
            _defaultLog.Mark(name);
        }

        [Conditional("PROFILE")]
        public static void Leave()
        {
            _defaultLog.Leave();
        }

        [Conditional("PROFILE")]
        public static void Stop()
        {
            _defaultLog.Stop();
        }
    }

    public class ProfileLog
    {
        public ProfileLog(string filename)
        {
            _filename = filename;
        }

        string _filename;

        [Conditional("PROFILE")]
        public void Enter(string name)
        {
            Write(new ProfileRecord()
            {
                type = ProfileRecordType.Enter,
                strArg = name,
                lArg = Stopwatch.GetTimestamp(),
            });
        }

        [Conditional("PROFILE")]
        public void Tag(string name)
        {
            Write(new ProfileRecord()
            {
                type = ProfileRecordType.Tag,
                strArg = name,
                lArg = Stopwatch.GetTimestamp(),
            });
        }

        [Conditional("PROFILE")]
        public void Mark(string name)
        {
            // Maybe one day

            // The idea here is to be able to insert marks inside a scope
            // and get a breakdown of time passed between each mark

            throw new NotImplementedException(); 

            /*
            Write(new ProfileRecord()
            {
                type = ProfileRecordType.Mark,
                strArg = name,
                lArg = Stopwatch.GetTimestamp(),
            });
            */
        }

        [Conditional("PROFILE")]
        public void Leave()
        {
            Write(new ProfileRecord()
            {
                type = ProfileRecordType.Leave,
                lArg = Stopwatch.GetTimestamp(),
            });
        }

        [Conditional("PROFILE")]
        public void Stop()
        {
            if (_writerThread != null)
            {
                _stopFlag = true;
                _writerThread.Join();
                _writerThread = null;
            }
        }

        class Queue : ConcurrentQueue<ProfileRecord>
        {
            public Queue()
            {
                threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            }

            public int threadId;
        }

        private ThreadLocal<Queue> _queue = new ThreadLocal<Queue>();

        // Global list of all queues feeding into the writer thread
        List<Queue> _allQueues = new List<Queue>();

        // The writer thread
        System.Threading.Thread _writerThread;
        volatile bool _stopFlag;

        void Write(ProfileRecord r)
        {
            // First call on this thread?  Create a queue for it...
            if (!_queue.IsValueCreated)
            {
                _queue.Value = new Queue();

                lock (_allQueues)
                {
                    Console.WriteLine("Starting profile logging...");
                    _allQueues.Add(_queue.Value);
                    if (_allQueues.Count == 1)
                    {
                        _stopFlag = false;
                        _writerThread = new System.Threading.Thread(WriterThread);
                        _writerThread.IsBackground = true;
                        _writerThread.Start();
                    }


                    _queue.Value.Enqueue(new ProfileRecord()
                    {
                        type = ProfileRecordType.Frequency,
                        lArg = Stopwatch.Frequency,
                    });
                }
            }

            // Enqueue
            _queue.Value.Enqueue(r);
        }

        int _iQueueIndex = 0;
        Queue GetLoadedQueue()
        {
            while (!_stopFlag)
            {
                lock (_allQueues)
                {
                    // Round robin until we find one
                    while (true)
                    {
                        // Remember where we started
                        int startIndex = _iQueueIndex;

                        // Get the queue
                        var queue = _allQueues[_iQueueIndex];
                        if (!queue.IsEmpty)
                            return queue;

                        // Move to next queue
                        _iQueueIndex++;
                        if (_iQueueIndex >= _allQueues.Count)
                            _iQueueIndex = 0;

                        // If nothing, pause for a bit
                        if (_iQueueIndex == startIndex)
                        {
                            break;
                        }
                    }
                }

                // Sleep a while
                System.Threading.Thread.Sleep(1);
            }

            return null;
        }


        void WriterThread()
        {
            // Create the file
            using (var file = System.IO.File.Create(_filename))
            using (var br = new System.IO.BinaryWriter(file, Encoding.UTF8, true))
            {

                Dictionary<string, int> _stringMap = new Dictionary<string, int>();
                int lastThreadId = -1;

                while (!_stopFlag)
                {
                    var queue = GetLoadedQueue();
                    if (queue == null)
                        break;
                    
                    // Different thread, write the context switch
                    if (queue.threadId != lastThreadId)
                    {
                        br.Write((short)ProfileRecordType.ThreadID);
                        br.Write((long)queue.threadId);
                        lastThreadId = queue.threadId;
                    }

                    // Flush all records
                    ProfileRecord r;
                    while (queue.TryDequeue(out r))
                    {
                        // Do we need to allocate a string ID?
                        int strId = -1;
                        if (r.strArg != null)
                        {
                            if (!_stringMap.TryGetValue(r.strArg, out strId))
                            {
                                strId = _stringMap.Count + 1;
                                _stringMap.Add(r.strArg, strId);

                                // Write the string id record
                                br.Write((short)ProfileRecordType.StringID);
                                br.Write(strId);
                                br.Write(r.strArg);
                            }
                        }

                        // Write the record
                        switch (r.type)
                        {
                            case ProfileRecordType.Enter:
                            case ProfileRecordType.Tag:
                            case ProfileRecordType.Mark:
                                br.Write((short)r.type);
                                br.Write(strId);
                                br.Write(r.lArg);
                                break;

                            case ProfileRecordType.Leave:
                            case ProfileRecordType.Frequency:
                                br.Write((short)r.type);
                                br.Write(r.lArg);
                                break;
                        }
                    }
                }
            }
        }
    }


    public enum ProfileRecordType
    {
        // Not used in in-memory queue (disk only)
        ThreadID,           // <int threadId>
        StringID,           // <int id>, <utf8 string>

        // Record types 
        Frequency,          // lArg = frequency of ticks
        Enter,              // <int string id>, <long time in ticks>
        Leave,              // <long time in ticks>
        Tag,                // <int string id>, <long time in ticks>
        Mark,               // <int string id>, <long time in ticks>
    }

    public struct ProfileRecord
    {
        public ProfileRecordType type;
        public string strArg;
        public long lArg;
    }

}
