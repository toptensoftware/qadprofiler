﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QadProfiler
{
    public class ProfileScope
    {
        public ProfileScope(ProfileScope superScope, string name, bool tagScope)
        {
            _superScope = superScope;
            _name = name;
            _isTagScope = tagScope;
        }

        public ProfileScope GetSubScope(string name, bool tagScope)
        {
            ProfileScope subScope;
            if (!_subScopeMap.TryGetValue(name, out subScope))
            {
                subScope = new ProfileScope(this, name, tagScope);
                _subScopeMap.Add(name, subScope);
                _subScopeList.Add(subScope);
            }
            System.Diagnostics.Debug.Assert(subScope.IsTagScope == tagScope);
            return subScope;
        }


        public bool IsTagScope
        {
            get
            {
                return _isTagScope;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string DisplayName
        {
            get
            {
                if (IsTagScope)
                    return string.Format("[{0}]", _name);
                else
                    return _name;
            }
        }

        public ProfileScope SuperScope
        {
            get { return _superScope; }
        }

        public IEnumerable<ProfileScope> SubScopes
        {
            get { return _subScopeList; }
        }

        public long ElapsedTime
        {
            get { return _elapsedTime; }
        }

        public long ExclusiveTime
        {
            get { return _exclusiveTime; }
        }

        long ExclusiveTimeDeduction
        {
            get
            {
                if (_isTagScope)
                    return _subScopeList.Sum(x => x.ExclusiveTimeDeduction);
                else
                    return _elapsedTime;
            }
        }

        internal void UpdateExclusiveFromElapsed(bool recursive)
        {
            _exclusiveTime = _elapsedTime - _subScopeList.Sum(x => x.ExclusiveTimeDeduction);

            if (recursive)
            {
                foreach (var ss in _subScopeList)
                {
                    ss.UpdateExclusiveFromElapsed(true);
                }
            }
        }

        internal void UpdateElapsedFromExclusive(bool recursive)
        {
            _elapsedTime = _exclusiveTime;
            if (recursive)
            {
                foreach (var ss in _subScopeList)
                {
                    ss.UpdateElapsedFromExclusive(true);
                }
            }
            _elapsedTime += _subScopeList.Sum(x=>x.ElapsedTime);
        }

        public long Count
        {
            get { return _count; }
        }

        public void Enter(long time)
        {
            _enterTime = time;
        }

        public void CoalescTags()
        {
            // Coalesc all sub-scopes first
            foreach (var ss in _subScopeList)
            {
                ss.CoalescTags();
            }

            var tagScopes = _subScopeList.Where(x => x.IsTagScope).ToList();
            if (tagScopes.Count > 0)
            {
                // Reset this scope and rebuild from tag sub scopes
                _count = 0;

                foreach (var s in tagScopes)
                {
                    this.CoalescWith(s, (ss) => true);
                    _subScopeMap.Remove(s.Name);
                    _subScopeList.Remove(s);
                }
            }
        }

        public void CoalescWith(ProfileScope other, Func<ProfileScope, bool> recurseCheck)
        {
            if (_count == 0)
            {
                _fastest = other._fastest;
                _slowest = other._slowest;
                _exclusiveTime = other._exclusiveTime;
                _count = other._count;
            }
            else
            {
                _fastest = Math.Min(_fastest, other._fastest);
                _slowest = Math.Max(_slowest, other._slowest);
                _exclusiveTime += other._exclusiveTime;
                _count += other._count;
            }

            if (!recurseCheck(other))
                return;

            foreach (var ssSrc in other.SubScopes)
            {
                var ssDest = this.GetSubScope(ssSrc._name, ssSrc._isTagScope);
                ssDest.CoalescWith(ssSrc, recurseCheck);
            }
        }

        public void RegisterMark(string name, long time)
        {
        }

        public void Leave(long time)
        {
            var elapsed = time - _enterTime;

            if (_count == 0)
            {
                _fastest = elapsed;
                _slowest = elapsed;
            }
            else
            {
                _fastest = Math.Min(_fastest, elapsed);
                _slowest = Math.Max(_fastest, elapsed);
            }

            _elapsedTime += elapsed;
            _count++;
        }

        internal void FinalizePseudoScope(long lastRecordedTime)
        {
            _elapsedTime = lastRecordedTime;
            _exclusiveTime = 0;
        }

        ProfileScope _superScope;
        string _name;
        bool _isTagScope;
        List<ProfileScope> _subScopeList = new List<ProfileScope>();
        Dictionary<string, ProfileScope> _subScopeMap = new Dictionary<string, ProfileScope>();

        internal long _fastest;
        internal long _slowest;
        internal long _elapsedTime;
        internal long _exclusiveTime;
        internal long _count;

        internal long _enterTime;
    }
}
