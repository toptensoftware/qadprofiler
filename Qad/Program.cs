﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QadProfiler
{
    class Program
    {
        #region Time Formatting
        static long frequency;      // For converting ticks to realtime
        static long totalTime;      // Total elapsed time for percentage calcs

        static string FormatTimeMS(long ticks)
        {
            return string.Format("{0:0.000}", (double)ticks * 1000.0 / frequency);
        }

        static string FormatTimeTicks(long ticks)
        {
            return ticks.ToString();
        }

        static string FormatTimePercent(long ticks)
        {
            return string.Format("{0:0.000}%", ticks * 100.0 / totalTime);
        }

        static Func<long, string> FormatTime = FormatTimeMS;
        #endregion

        #region Scope Dumping

        // Used for indentation
        static string spaces = new string(' ', 512);
        static string DumpFormat = "{1,10} {2,10} {3,10} {4,10} {5,10}  {0}";

        static void Dump(ProfileScope scope, int depth)
        {
            Console.WriteLine(DumpFormat, 
                    spaces.Substring(0, depth) + scope.DisplayName, 
                    FormatTime(scope.ExclusiveTime), 
                    FormatTimePercent(scope.ExclusiveTime),
                    FormatTime(scope.ElapsedTime), 
                    FormatTimePercent(scope.ElapsedTime),
                    scope.Count);
            var saveTotalTime = totalTime;
            if (depth == 0)
                totalTime = scope.ElapsedTime;
            foreach (var ss in scope.SubScopes)
            {
                Dump(ss, depth + 1);
            }
            totalTime = saveTotalTime;
        }
        #endregion

        static void ShowUsage()
        {
            Console.WriteLine("Usage: qad [--ticks|--ms] [--calltree] [--tags] [--raw] [<QadLog.bin>]");
        }

        static void Main(string[] args)
        {
            bool dumpCallTree = false;
            bool coalescTags = true;
            bool rawDump = false;

            // Parse command line
            string file = null;
            foreach (var a in args)
            {
                switch (a)
                {
                    case "--ticks":
                        FormatTime = FormatTimeTicks;
                        break;

                    case "--ms":
                        FormatTime = FormatTimeMS;
                        break;

                    case "--calltree":
                        dumpCallTree = true;
                        break;

                    case "--tags":
                        coalescTags = false;
                        break;

                    case "--raw":
                        rawDump = true;
                        break;

                    default:
                        file = a;
                        break;
                }
            }

            // Check have a file?
            if (file == null)
            {
                if (System.IO.File.Exists("QadLog.bin"))
                {
                    file = "QadLog.bin";
                }
                else
                {
                    ShowUsage();
                    return;
                }
            }

            // Read the file
            ProfileScope rootScope;
            using (var pr = new ProfileReader(file))
            {
                // Build raw scope graph
                var pb = new ScopeBuilder();
                rootScope = new ProfileScope(null, "/", false);
                pb.Build(rootScope, pr);
                frequency = pb.Frequency;
                totalTime = pr.RunLength;
            }
            rootScope.UpdateExclusiveFromElapsed(true);

            // Dump the basics
            Console.WriteLine("\nTimer Frequency: {0}", frequency);
            Console.WriteLine("Run Length: {0}ms", FormatTimeMS(totalTime));

            // Headers
            Console.WriteLine();
            Console.WriteLine(DumpFormat, "Scope", "Excl.", "Excl.%", "Elap.", "Elap.%", "Count");
            Console.WriteLine(new string('-', 115));

            // Raw dump?
            if (rawDump)
            {
                Dump(rootScope, 0);
                return;
            }

            // Collapse tags
            if (coalescTags)
            {
                rootScope.CoalescTags();
                rootScope.UpdateElapsedFromExclusive(true);
            }

            // Combine all thread scopes
            var combinedThreadScope = new ProfileScope(null, "thread.*", false);
            foreach (var ts in rootScope.SubScopes)
            {
                combinedThreadScope.CoalescWith(ts, (x) => true);
            }
            combinedThreadScope.UpdateElapsedFromExclusive(true);

            // Jump dumping the call tree?
            if (dumpCallTree)
            {
                Dump(combinedThreadScope, 0);
                return;
            }

            // Create the summary scope
            var summaryScope = new ProfileScope(null, "summary", false);

            // Build root scope list
            Dictionary<string, ProfileScope> _rootScopes = new Dictionary<string, ProfileScope>();
            foreach (var rs in combinedThreadScope.SubScopes)
            {
                ProfileScope scope;
                if (!_rootScopes.TryGetValue(rs.Name, out scope))
                {
                    scope = summaryScope.GetSubScope(rs.Name, rs.IsTagScope);
                    _rootScopes.Add(rs.Name, scope);
                }
            }

            // Coalesc all root scopes
            var listRootScopes = combinedThreadScope.SubScopes.ToList();
            int i = 0;
            while (i<listRootScopes.Count)
            {
                var rs = listRootScopes[i];
                _rootScopes[rs.Name].CoalescWith(rs, (ss) =>
                {
                    if (ss == rs)
                        return true;
                    if (ss.Name != rs.Name)
                        return true;
                    listRootScopes.Add(ss);
                    return false;
                });
                i++;
            }

            // Dump it
            summaryScope.UpdateElapsedFromExclusive(true);
            Dump(summaryScope, 0);
        }
    }
}
