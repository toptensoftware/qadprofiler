﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QadProfiler
{
    public class ScopeBuilder
    {
        class ThreadState
        {
            public ThreadState(ProfileScope rootScope, long threadId)
            {
                _threadRootScope = rootScope.GetSubScope(string.Format("thread.{0}", threadId), false);
                _currentScope = _threadRootScope;
            }

            ProfileScope _threadRootScope;
            ProfileScope _currentScope;

            public ProfileScope ThreadScope
            {
                get { return _threadRootScope; }
            }

            public void Append(ProfileRecord record)
            {
                switch (record.type)
                {
                    case ProfileRecordType.Enter:
                        // Get/Create subscope
                        _currentScope = _currentScope.GetSubScope(record.strArg, false);

                        // Record entry time
                        _currentScope.Enter(record.lArg);
                        break;

                    case ProfileRecordType.Leave:
                        // Leave originally entered scope
                        while (_currentScope.IsTagScope)
                        {
                            System.Diagnostics.Debug.Assert(_currentScope != _threadRootScope);
                            _currentScope.Leave(record.lArg);
                            _currentScope = _currentScope.SuperScope;
                        }

                        System.Diagnostics.Debug.Assert(_currentScope != _threadRootScope);
                        _currentScope.Leave(record.lArg);
                        _currentScope = _currentScope.SuperScope;
                        break;

                    case ProfileRecordType.Mark:
                        _currentScope.RegisterMark(record.strArg, record.lArg);
                        break;

                    case ProfileRecordType.Tag:
                        // Get/Create subscope
                        var enterTime = _currentScope._enterTime;
                        _currentScope = _currentScope.GetSubScope(record.strArg, true);

                        // Record entry time
                        _currentScope.Enter(enterTime);
                        break;
                }
            }
        }

        public long Frequency;

        public void Build(ProfileScope root, ProfileReader reader)
        {
            var threadStates = new Dictionary<long, ThreadState>();

            ThreadState currentThread = null;

            ProfileRecord r;
            while (reader.Next(out r))
            {
                switch (r.type)
                {
                    case ProfileRecordType.ThreadID:
                        if (!threadStates.TryGetValue(r.lArg, out currentThread))
                        {
                            currentThread = new ThreadState(root, r.lArg);
                            threadStates.Add(r.lArg, currentThread); 
                        }
                        break;

                    case ProfileRecordType.Frequency:
                        Frequency = r.lArg;
                        break;

                    default:
                        System.Diagnostics.Debug.Assert(currentThread != null);
                        currentThread.Append(r);
                        break;
                }
            }

            // Calculate all exclusive times from elapsed times
            foreach (var ts in threadStates.Values)
            {
                ts.ThreadScope.UpdateExclusiveFromElapsed(true);
                ts.ThreadScope.FinalizePseudoScope(ts.ThreadScope.SubScopes.Sum(x => x.ElapsedTime));
            }
        }
    }
}
