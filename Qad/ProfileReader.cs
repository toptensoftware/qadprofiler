﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QadProfiler
{
    public class ProfileReader : IDisposable
    {
        public ProfileReader(string filename)
        {
            _file = File.OpenRead(filename);
            _reader = new BinaryReader(_file, Encoding.UTF8, true);
        }

        public bool Next(out ProfileRecord record)
        {
            if (_file.Position >= _file.Length)
            {
                record = new ProfileRecord();
                return false;
            }

            try
            {
                // Read the record type
                var rt = (ProfileRecordType)_reader.ReadInt16();

                // Handle strings automatically
                if (rt == ProfileRecordType.StringID)
                {
                    int id = _reader.ReadInt32();
                    string str = _reader.ReadString();
                    _stringIDs.Add(id, str);
                    return Next(out record);
                }

                record.type = rt;
                switch (rt)
                {
                    case ProfileRecordType.Enter:
                    case ProfileRecordType.Mark:
                    case ProfileRecordType.Tag:
                        record.strArg = _stringIDs[_reader.ReadInt32()];
                        record.lArg = _reader.ReadInt64();
                        break;

                    case ProfileRecordType.ThreadID:
                    case ProfileRecordType.Frequency:
                    case ProfileRecordType.Leave:
                        record.strArg = null;
                        record.lArg = _reader.ReadInt64();
                        break;

                    default:
                        throw new InvalidDataException("Unrecognoized record type in perf log");
                }

                if (_firstTime == 0 && rt == ProfileRecordType.Enter)
                {
                    _firstTime = record.lArg;
                }
                if (rt == ProfileRecordType.Leave)
                {
                    _lastTime = record.lArg;
                }


                return true;
            }
            catch (EndOfStreamException)
            {
                record = new ProfileRecord();
                _file.Position = _file.Length;
                return false;
            }
        }

        FileStream _file;
        BinaryReader _reader;
        Dictionary<int, string> _stringIDs = new Dictionary<int, string>();
        long _firstTime;
        long _lastTime;

        public long RunLength
        {
            get { return _lastTime - _firstTime; }
        }

        public void Dispose()
        {
            if (_file != null)
            {
                _reader.Dispose();
                _file.Dispose();
                _file = null;
                _reader = null;
            }
        }
    }
}
